# Hidde Westerhof 2016/10/14
from termcolor import colored

class Node:

    def __init__(self, name, light = 0):
        self.name = name
        self.light = light
        self.neighbours = []
        self.vehicle = []

    def addNeighbour(self, node):
        self.neighbours.append(node)

    def setCoords(self, coords):
        self.x = coords[0]
        self.y = coords[1]

    def setLight(self, vehicle):
        if self.light != 0:
            if self.light.getTraffic() == 0:
                self.light.setTraffic(vehicle)

    def addVehicle(self, vehicle):
        self.vehicle.append(vehicle)
        self.setLight(vehicle)

    def rmVehicle(self, vehicle):
        self.vehicle.remove(vehicle)
        if self.light != 0:
            self.light.setTraffic(0)

    def getVehicle(self):
        if self.light == 0:
            return self.vehicle[0]
        else:
            return self.light.getTraffic()

    def getLight(self):
        return self.light

    def toStr(self):
        if self.light == 0:#if not light, show car stuff
            if len(self.vehicle) == 0: #if no cars
                return " "
            else:
                return "{}".format(self.vehicle[0].toStr())
        else:
            return self.light.toStr()

class Road(Node):
    def __init__(self, name, light = 0):
        self.name = name
        self.light = light
        self.neighbours = []
        self.vehicle = []
        return

    def toStr(self):
        if self.light == 0:#if not light, show car stuff
            if len(self.vehicle) == 0: #if no cars
                return " "
            else:
                return "{}".format(self.vehicle[0].toStr())
        else:
            return self.light.toStr()

class BuLane(Road):
    def toStr(self):
        if self.light == 0:#if not light, show car stuff
            if len(self.vehicle) == 0: #if no cars
                return " "
            else:
                return "{}".format(self.vehicle[0].toStr())
        else:
            return self.light.toStr()

class Rail(Node):
    def __init__(self, name, light = 0):
        self.name = name
        self.light = light
        self.neighbours = []
        self.vehicle = []
        return

    def toStr(self):
        if len(self.vehicle) != 0:
            return "{}".format(self.vehicle[0].toStr())
        return "="

class SWalk(Node):
    def toStr(self):
        if self.light == 0:#if not light, show car stuff
            if len(self.vehicle) == 0: #if no cars
                return " "
            else:
                return "{}".format(self.vehicle[0].toStr())
        else:
            return self.light.toStr()

class BiLane(Node):
    def toStr(self):
        if self.light == 0:#if not light, show car stuff
            if len(self.vehicle) == 0: #if no cars
                return " "
            else:
                return "{}".format(self.vehicle[0].toStr())
        else:
            return self.light.toStr()
