# Hidde Westerhof 2016/10/14
from sim import Simulator
from wsclient import WSocketMaster

import multiprocessing

if __name__ == "__main__":
    #create queue
    queue = multiprocessing.JoinableQueue()
    #create websocket process
    p_crwler = WSocketMaster(queue)
    p_crwler.start()
    #queue.put("The queue is a sucess!")
    s = Simulator(queue)
    try:
        s.run()
    except KeyboardInterrupt:
        os.system('clear')
        #join
        #destroy queue
        pass
