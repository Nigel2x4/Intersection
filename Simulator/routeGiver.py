import random
import copy

class RouteGiver:

    biRoutes =[
                ["BL1", "SWC" , "BLC", "BL2", "BL3","BL4","BL5", "BL6", "RAIL2", "BL7"],
                ["BLA", "BLB", "BLC", "BLD", "BLE", "BLF", "BLG", "BLH", "BLI", "BLJ", "BLK"],
                ["BL15", "SWI","BLI","BL14","BL13","BL12","BL11","BL10","RAIL8","BL9"]
    ]
    wRoutes = [
                ["SWA", "SWB", "SWC", "SWD", "SWE","SWF","SWG", "SWH", "SWI", "SWJ", "SWK", "SWL", "SWM", "SWN","SWO","SWP","SWQ","SWR","SWS","SWT","SWU"],
                ["SW1", "SW2", "SW3","SWE", "BLE", "SW4", "SW5", "SW6", "SW7", "SW8", "SW9", "SW10", "SW11", "SW12", "SW13", "RAIL4", "SW14", "SW15","SW16","SW17"],
                ["SW20","SW21","SW22","SWP","BLP","SW23","SW24","SW25","SW26","SW27","SW28","SW29","SW30","SW31","SW32","RAILF","SW33","SW34","SW35","SW36"]
    ]
    #busses can com FROM station (WEST) or go to station (WEST)
    buRoutes = [
        #west beginnings
        ["WBA1", "WBA2", "WBA3", "WBA4", "SW7", "BL7", "D1", "D2", "D3", "D4", "D5", "D6", "C7", "B7", "A7", "BLM", "SWM", "NEA1", "NEA2", "NEA3"],
        ["WBB1", "WBB2", "WBB3", "WBB4", "SW8", "BL8", "E1", "E2", "E3", "E4", "E5", "E6", "E7", "F8", "BL28", "SW28", "OEA1", "OEA2", "OEA3", "OEAD", "OEAE"],
        ["WBB1", "WBB2", "WBB3", "WBB4", "SW8", "BL8", "E1", "E2", "E3", "E4", "E5", "F6", "F7", "F8", "BL29", "SW29", "OEAA", "OEAB", "OEAC", "OEAD", "OEAE"],
        ["WBC1", "WBC2", "WBC3", "WBC4", "SW9", "BL9", "F1", "F2", "G3", "ZEA1", "ZEA2", "ZEA3", "RAIL8", "ZEA4", "ZEA5", "ZEA6", "ZEA7"],
        #north beginning
        ["NBA3", "NBA2", "NBA1", "SWH", "BLH", "A2", "B1",  "BL5", "SW5",  "WBE1", "WBE2", "WBE3","WBE4"],
        #east beginning
        ["OBB5", "OBB4","OBB3", "OBB2", "OBB1", "SW24", "BL24", "B8", "B7", "B6", "B5", "B4","B3", "B2", "B1", "BL5", "SW5", "WBE1", "WBE2", "WBE3","WBE4"],
        #south beginning
        ["ZBA1", "ZBA2","ZBA3""ZBA4", "RAILA", "ZBA5", "ZBA6", "ZBA7" "G5", "F5", "E5", "D4", "C3", "B2", "B1", "BL5", "SW5", "WBE1", "WBE2", "WBE3","WBE4"]
    ]

    cRoutes = [
        #west beginnings
        ["WBA1", "WBA2", "WBA3", "WBA4", "SW7", "BL7", "D1", "D2", "D3", "D4", "D5", "D6", "C7", "B7", "A7", "BLM", "SWM", "NEA1", "NEA2", "NEA3"],
        ["WBB1", "WBB2", "WBB3", "WBB4", "SW8", "BL8", "E1", "E2", "E3", "E4", "E5", "E6", "E7", "F8", "BL28", "SW28", "OEA1", "OEA2", "OEA3", "OEAD", "OEAE"],
        ["WBB1", "WBB2", "WBB3", "WBB4", "SW8", "BL8", "E1", "E2", "E3", "E4", "E5", "F6", "F7", "F8", "BL29", "SW29", "OEAA", "OEAB", "OEAC", "OEAD", "OEAE"],
        ["WBC1", "WBC2", "WBC3", "WBC4", "SW9", "BL9", "F1", "F2", "G3", "ZEA1", "ZEA2", "ZEA3", "RAIL8", "ZEA4", "ZEA5", "ZEA6", "ZEA7"],
        #north beginnings
        ["NBA3", "NBA2", "NBA1", "SWH", "BLH", "A2", "B1",  "BL5", "SW5",  "WBE1", "WBE2", "WBE3","WBE4"],
        ["NBB3", "NBB2", "NBB1", "SWI", "BLI", "A3", "B3", "C3", "D3", "E3", "F3", "G3", "ZEA1", "ZEA2", "ZEA3", "RAIL8", "ZEA4", "ZEA5", "ZEA6", "ZEA7"],
        ["NBC3", "NBC2", "NBC1", "SWJ", "BLJ", "A4", "B4", "C4", "D5", "E6", "F6", "F7", "F8", "BL28", "SW28", "OEA1", "OEA2", "OEA3", "OEAD", "OEAE"],
        ["NBD3", "NBD2", "NBD1", "SWK", "BLK", "A5", "B5", "C5", "E6", "F7", "G8", "BL29", "SW29", "OEAA", "OEAB", "OEAC", "OEAD", "OEAE"],
        #east beginnings
        ["OBA5", "OBA4", "OBA3","OBA2","OBA1", "SW25", "BL25", "B8", "A7", "BLM", "SWM", "NEA1", "NEA2", "NEA3"],
        ["OBA5", "OBA4", "OBA3","OBA2","OBA1", "SW25", "BL25", "B8", "B7", "B6", "B5", "B4", "B3", "B2", "B1",  "BL5", "SW5", "WBE1", "WBE2","WBE3","WBE4"],
        #south beginnings
        ["ZBA1", "ZBA2", "ZBA3", "ZBA4" "RAILA", "ZBA5", "ZBA6", "ZBA7", "G5", "F5", "E4", "D3", "C2", "B1", "BL5", "SW5", "WBE1", "WBE2", "WBE3", "WBE4"],
        ["ZBB1", "ZBB2", "ZBB3", "ZBB4" "RAILB", "ZBB5", "ZBB6", "ZBB7", "G6", "F7", "E7", "D7", "C7", "B7", "A7", "BLM", "SWM", "NEA1", "NEA2", "NEA3"],
        ["ZBB1", "ZBB2", "ZBB3", "ZBB4" "RAILB", "ZBB9", "ZBBD", "ZBBC", "BL29", "SW29", "OEAA", "OEAB", "OEAC", "OEAD", "OEAE"]
    ]

    def __init__(self):
        return None

    def getCopy(self, alist):
        return copy.copy(alist[random.randrange(len(alist))])

    def getRandom(self, value = 11):
        return random.randrange(value)

    def getCarRoute(self):
        return self.getCopy(self.cRoutes)

    def getBusRoute(self):
        return self.getCopy(self.buRoutes)

    def getBikeRoute(self):
        if self.getRandom() > 5:
            return self.getCopy(self.biRoutes)
        else:
            return self.getCopy(self.biRoutes)[::-1]

    def getWalkRoute(self):
        if self.getRandom() > 5:
            return self.getCopy(self.wRoutes)
        else:
            return self.getCopy(self.wRoutes)[::-1]
