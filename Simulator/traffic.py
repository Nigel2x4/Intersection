# Hidde Westerhof 2016/10/14

import random

from termcolor import colored

class Traffic:

    blocks = True

    def __init__(self, idl, route = []):
        self.id = str(idl)
        self.route = []
        self.route = route
        self.location = 0
        self.dead = False
        self.dest = route[-1]

    def getRoute(self):
        return self.route

    def setLocation(self, value):
        self.location = value

    def toggleTravel(self):
        self.traveled = not self.traveled

    def travel(self):
        n = self.location
        if self.dead == True:
            return
        if len(self.route) == 0:
            return
        if n.light != 0:
            if n.light.getLight() == 0:
                #print("red light")
                return
        for nei in n.neighbours:
            if nei.name == self.route[0]:
                #print(nei.name)
                if len(nei.vehicle) == 0: #if there are no vehicles in the list on the target node.
                    if nei.name == self.dest: #if destination
                        n.rmVehicle(self)
                        return True
                    nei.addVehicle(self)
                    self.route.pop(0)
                    n.rmVehicle(self)
                    self.location = nei
                    return
                else: #if there are vehicles on that node.
                    if not nei.getVehicle().blocks or not self.blocks: #check if collide.
                        if nei.name == self.dest: # if destination
                            n.rmVehicle(self)
                            return True
                        nei.addVehicle(self) #move there
                        self.route.pop(0)
                        n.rmVehicle(self)
                        self.location = nei
                        return
                    else:
                        #self.dead = True
                        return

    def toStr(self):
        raise NotImplementedError

class Car(Traffic):

    __colors = ["red","yellow","blue","white","cyan","green"]

    def __init__(self, idl, route = []):
        self.id = str(idl)
        self.route = []
        self.route = route
        self.location = 0
        self.dead = False
        self.dest = route[-1]
        self.color = self.__colors[random.randrange(len(self.__colors))]

    def toStr(self):
        return colored("A", self.color)

class Pedestrian(Traffic):

    blocks = False

    def toStr(self):
        return colored("V", 'blue')

class Bike(Traffic):

    blocks = False

    def travel(self):
        n = self.location
        if self.dead == True:
            return
        if len(self.route) == 0:
            return
        if n.light != 0:
            if n.light.getLight() == 0:
                #print("red light")
                return
        for nei in n.neighbours:
            if nei.name == self.route[0]:
                if nei.name == self.dest:
                    n.vehicle.pop(0)
                    return True
                nei.vehicle.append(self)
                self.route.pop(0)
                n.vehicle.pop(0)
                self.location = nei
                return

    def toStr(self):
        return colored("F", 'white')

class Bus(Traffic):

    def toStr(self):
        return colored("B", 'cyan')

class Train(Traffic):
    def __init__(self, idl):
        self.location = 0
        self.dead = False
        self.route = []
        for i in range(11):
            self.route.append("RAIL{}".format(str(i)))
        self.dest = self.route[-1]

    def toStr(self):
        return colored("T", 'yellow')
