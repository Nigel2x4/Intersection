
from field import Field
from traffic import *
from routeGiver import RouteGiver

import random
import time
import os
import copy

class Simulator:

        __RNG_MIN = 0.1
        __RNG_MAX = 100.0
        __UPDATE_SPEED_SECONDS = 0.25
        __TRN_MAX_SPAWN_VALUE = 0.5
        __CAR_MAX_SPAWN_VALUE = 60
        __BUS_MAX_SPAWN_VALUE = 65
        __PED_MAX_SPAWN_VALUE = 80
        __WIDTH = 20
        __HEIGHT = 21

        def __init__(self, queue):
            self.__field = Field(self.__WIDTH, self.__HEIGHT)
            self.__planner = RouteGiver()
            self.__traffic = []
            self.__queue = queue
            return

        def getRandom(self):
            return random.uniform(self.__RNG_MIN, self.__RNG_MAX)

        def create(self):
            addval = []
            dice = self.getRandom()
            dice1 = random.randrange(0,2)
            if dice1 == 1:

                # if dice < self.__TRN_MAX_SPAWN_VALUE: #train
                #     addval.extend([Train(0),Train(0),Train(0),Train(0)])
                if dice < self.__CAR_MAX_SPAWN_VALUE: #car
                    addval.append(Car(1, self.__planner.getCarRoute()))

                elif dice < self.__BUS_MAX_SPAWN_VALUE: #bus
                    addval.append(Bus(1, self.__planner.getBusRoute()))

                elif dice < self.__PED_MAX_SPAWN_VALUE:  #pedestrian
                    addval.append(Pedestrian(1, self.__planner.getWalkRoute()))
                
                # else: #bike
                #     addval.append(Bike(1, self.__planner.getBikeRoute()))
                for t in addval:
                    t.location = self.__field.getNode(t.getRoute()[0])
                    self.__field.getNode(t.getRoute()[0]).addVehicle(t)
                    t.getRoute().pop(0)
                self.__traffic.extend(addval)

        def update(self):
            self.showArea()
            while True:
                if not self.__queue.empty():
                    obj = self.__queue.get(False)
                    if obj["dest"] == "sim":
                        self.__field.setStatus(obj["data"])
                    else:
                        self.__queue.put(obj)
                time.sleep(self.__UPDATE_SPEED_SECONDS)
                os.system('clear')
                for t in self.__traffic:
                    #print(type(t))
                    if t.travel():
                        self.__traffic.remove(t)
                self.showArea()
                self.create()
                self.__queue.put({"dest": "ws" , "data": self.__field.getStatus()})

        def showArea(self):
            for x in range(self.__WIDTH):
                for y in range(self.__HEIGHT):
                    print(self.__field.getValue(x, y), end = '')
                print("")

        def run(self):
            self.update()
