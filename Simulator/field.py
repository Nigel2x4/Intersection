# Hidde Westerhof 2016/10/14

import json

from node import *
from light import *

class Field:

    trafficLightlist = []

    def setStatus(self, data):
        # print(type(data))
        if data == "":
            return
        data = json.loads(data)
        for light in data["state"]:
            for x in self.field:
                for node in x:
                    if node.getLight() != 0:
                        if node.getLight().getId() is light["trafficLight"]:
                            node.getLight().setLight(light["status"])
                            break

    def getStatus(self):
        retval = []
        for x in self.field:
            for y in x:
                if y.getLight() != 0:
                    #print("{}-{}:{}".format(y.x, y.y, y.getLight().getTrafficCount()))
                    retval.append({"trafficLight":y.getLight().getId(), "count":y.getLight().getTrafficCount()})

        return json.dumps({"state": retval})

    def setupLights(self):
        for i in range(1, 11):
            self.trafficLightlist.append(CarLight(i))

        for i in range(31, 39):
            self.trafficLightlist.append(BikeLight(i))

        for i in range(21, 28):
            self.trafficLightlist.append(PedLight(i))

        self.trafficLightlist.append(BusLight(42))
    #end setup lights




    field = [
                [Node("-"),     Node("-"),     Node("-"),     Node("-"),                    SWalk("SW1"),                   BiLane("BL1"),                  Node("-"),                  Road("NBA3"),               Road("NBB3"),               Road("NBC3"),                   Road("NBD3"),                   Node("-"),                   Road("NEA3"),     Node("-"),                     BiLane("BL20"),                 SWalk("SW20"),                 Node("-"),                  Node("-"),                     Node("-"),      Node("-"),      Node("-")],
                [Node("-"),     Node("-"),     Node("-"),     Node("-"),                    SWalk("SW2"),                   BiLane("BL2"),                  Node("-"),                  Road("NBA2"),               Road("NBB2"),               Road("NBC2"),                   Road("NBD2"),                   Node("-"),                   Road("NEA2"),     Node("-"),                     BiLane("BL21"),                 SWalk("SW21"),                 Node("-"),                  Node("-"),                     Node("-"),      Node("-"),      Node("-")],
                [Node("-"),     Node("-"),     Node("-"),     Node("-"),                    SWalk("SW3"),                   BiLane("BL3"),                  Node("-"),                  Road("NBA1",CarLight(8)),   Road("NBB1", CarLight(9)),  Road("NBC1", CarLight(10)),     Road("NBD1",CarLight(10)),      Node("-"),                   Road("NEA1"),     Node("-"),                     BiLane("BL22"),                 SWalk("SW22"),                 Node("-"),                  Node("-"),                     Node("-"),      Node("-"),      Node("-")],
                [SWalk("SWA"),  SWalk("SWB"),  SWalk("SWC"),  SWalk("SWD"),                 SWalk("SWE"),                   BiLane("SWF"),                  SWalk("SWG",PedLight(37)),  SWalk("SWH"),               SWalk("SWI"),               SWalk("SWJ"),                   SWalk("SWK"),                   SWalk("SWL"),                SWalk("SWM"),     SWalk("SWN",PedLight(38)),     SWalk("SWO"),                   SWalk("SWP"),                  SWalk("SWQ"),               SWalk("SWR"),                  SWalk("SWS"),   SWalk("SWT"),   SWalk("SWU")],
                [BiLane("BLA"), BiLane("BLB"), BiLane("BLC"), BiLane("BLD"),                BiLane("BLE"),                  BiLane("BLF"),                  BiLane("BLG",BikeLight(27)),BiLane("BLH"),              BiLane("BLI"),              BiLane("BLJ"),                  BiLane("BLK"),                  BiLane("BLL"),               BiLane("BLM"),    BiLane("BLN",BikeLight(28)),   BiLane("BLO"),                  BiLane("BLP"),                 BiLane("BLQ"),              BiLane("BLR"),                 BiLane("BLS"),  BiLane("BLT"),  BiLane("BLU")],
                [Node("-"),     Node("-"),     Node("-"),     Node("-"),                    SWalk("SW4", PedLight(36)),     BiLane("BL4",BikeLight(26)),    Road("A1"),                  Road("A2"),                Road("A3"),                 Road("A4"),                     Road("A5"),                     Road("A6"),                  Road("A7"),       Road("A8"),                    BiLane("BL23",BikeLight(21)),   SWalk("SW23", PedLight(31)),   Node("-"),                  Node("-"),                     Node("-"),      Node("-"),      Node("-")],
                [Road("WBE4"),  Road("WBE3"),  Road("WBE2"),  Road("WBE1"),                 SWalk("SW5"),                   BiLane("BL5"),                  Road("B1"),                 Road("B2"),                Road("B3"),                 Road("B4"),                     Road("B5"),                     Road("B6"),                  Road("B7"),        Road("B8"),                    BiLane("BL24"),                 SWalk("SW24"),                 Road("OBB1", BusLight(42)), Road("OBB2"),                  Road("OBB3"),   Road("OBB4"),   Road("OBB5")],
                [Node("-"),     Node("-"),     Node("-"),     Node("-"),                    SWalk("SW6"),                   BiLane("BL6"),                  Road("C1"),                  Road("C2"),                Road("C3"),                 Road("C4"),                     Road("C5"),                     Road("C6"),                  Road("C7"),       Road("C8"),                    BiLane("BL25"),                 SWalk("SW25"),                 Road("OBA1", CarLight(1)),  Road("OBA2"),                  Road("OBA3"),   Road("OBA4"),   Road("OBA5")],
                [Road("WBA1"),  Road("WBA2"),  Road("WBA3"),  Road("WBA4",CarLight(7)),     SWalk("SW7"),                   BiLane("BL7"),                  Road("D1"),                 Road("D2"),                 Road("D3"),                 Road("D4"),                     Road("D5"),                     Road("D6"),                  Road("D7"),       Road("D8"),                    BiLane("BL26"),                 SWalk("SW26"),                 Road("OBAA", CarLight(2)),  Road("OBAB"),                  Road("OBAC"),   Road("OBAD"),   Road("OBAE")],
                [Road("WBB1"),  Road("WBB2"),  Road("WBB3"),  Road("WBB4",CarLight(6)),     SWalk("SW8"),                   BiLane("BL8"),                  Road("E1"),                 Road("E2"),                 Road("E3"),                 Road("E4"),                     Road("E5"),                     Road("E6"),                  Road("E7"),       Road("E8"),                    BiLane("BL27"),                 SWalk("SW27"),                 Node("-"),                  Node("-"),                     Node("-"),      Node("-"),      Node("-")],
                [Road("WBC1"),  Road("WBC2"),  Road("WBC3"),  Road("WBC4",CarLight(5)),     SWalk("SW9"),                   BiLane("BL9"),                  Road("F1"),                 Road("F2"),                 Road("F3"),                 Road("F4"),                     Road("F5"),                     Road("F6"),                  Road("F7"),       Road("F8"),                    BiLane("BL28"),                 SWalk("SW28"),                 Road("OEA1"),               Road("OEA2"),                  Road("OEA3"),   Node("-"),      Node("-")],
                [Node("-"),     Node("-"),     Node("-"),     Node("-"),                    SWalk("SW10",PedLight(35)),     BiLane("BL10",BikeLight(25)),   Node("- "),                 Road("  "),                 Road("G3"),                 Road("G4"),                     Road("G5"),                     Road("G6"),                  Road("G7"),       Road("G8"),                    BiLane("BL29"),                 SWalk("SW29"),                 Road("OEAA"),               Road("OEAB"),                  Road("OEAC"),   Road("OEAD"),   Road("OEAE")],
                [BiLane("BL10"),BiLane("BL10"),BiLane("BL10"),BiLane("BL10"),               SWalk("SW11"),                  BiLane("BL11"),                 Road("- "),                 Road("  "),                 Road("ZEA1"),               Node("-"),                      Road("ZBA7"),                   Road("ZBB7"),                Road("ZBBB"),     Road("ZBBC"),                  BiLane("BL30",BikeLight(22)),   SWalk("SW30",PedLight(32)),    Node("-"),                  Node("-"),                     Node("-"),      Node("-"),      Node("-")],
                [SWalk("SW31"),  SWalk("SW32"),SWalk("SW33"), SWalk("SW34"),                SWalk("SW12"),                  BiLane("BL12"),                 Node("-"),                  Node("-"),                  Road("ZEA2"),               Node("-"),                      Road("ZBA6"),                   Node("ZBB6"),                Road("ZBBA"),     Road("ZBBD"),                  BiLane("BL31"),                 SWalk("SW31"),                 Node("-"),                  Node("-"),                     Node("-"),      Node("-"),      Node("-")],
                [Node("-"),     Node("-"),     Node("-"),     Node("-"),                    SWalk("SW13",PedLight(34)),     BiLane("BL13",BikeLight(24)),   Node("-"),                  Node("-"),                  Road("ZEA3"),               Node("-"),                      Road("ZBA5"),                   Node("ZBB5"),                Road("ZBB9"),     Node("-"),                     BiLane("BL32", BikeLight(23)),  SWalk("SW32", PedLight(33)),   Node("-"),                  Node("-"),                     Node("-"),      Node("-"),      Node("-")],
                [Rail("RAIL0"), Rail("RAIL1"), Rail("RAIL2"), Rail("RAIL3",RailLight(45)),  Rail("RAIL4"),                  Rail("RAIL5"),                  Rail("RAIL6"),              Rail("RAIL7"),              Rail("RAIL8"),              Rail("RAIL9"),                  Rail("RAILA"),                  Rail("RAILB"),               Rail("RAILC"),    Rail("RAILD"),                 Rail("RAILE"),                  Rail("RAILF"),                 Rail("RAILG",RailLight(46)),Rail("RAILH"),                 Rail("RAILI"), Rail("RAILJ"),  Rail("RAILK")],
                [Node("-"),     Node("-"),     Node("-"),     Node("-"),                    SWalk("SW14",PedLight(34)),     BiLane("BL14",BikeLight(24)),   Node("-"),                  Node("-"),                  Road("ZEA4"),               Node("-"),                      Road("ZBA4", CarLight(4)),      Road("ZBB4", CarLight(3)),   Node("-"),        Node("-"),                     BiLane("BL33", BikeLight(23)),  SWalk("SW33", PedLight(33)),   Node("-"),                  Node("-"),                     Node("-"),      Node("-"),      Node("-")],
                [Node("-"),     Node("-"),     Node("-"),     Node("-"),                    SWalk("SW15"),                  BiLane("BL15"),                 Node("-"),                  Node("-"),                  Road("ZEA5"),               Node("-"),                      Road("ZBA3"),                   Road("ZBB3"),                Node("-"),        Node("-"),                     BiLane("BL34"),                 SWalk("SW34"),                 Node("-"),                  Node("-"),                     Node("-"),      Node("-"),      Node("-")],
                [Node("-"),     Node("-"),     Node("-"),     Node("-"),                    SWalk("SW16"),                  BiLane("BL16"),                 Node("-"),                  Node("-"),                  Road("ZEA6"),               Node("-"),                      Road("ZBA2"),                   Road("ZBB2"),                Node("-"),        Node("-"),                     BiLane("BL35"),                 SWalk("SW35"),                 Node("-"),                  Node("-"),                     Node("-"),      Node("-"),      Node("-")],
                [Node("-"),     Node("-"),     Node("-"),     Node("-"),                    SWalk("SW17"),                  BiLane("BL17"),                 Node("-"),                  Node("-"),                  Road("ZEA7"),               Node("-"),                      Road("ZBA1"),                   Road("ZBB1"),                Node("-"),        Node("-"),                     BiLane("BL36"),                 SWalk("SW36"),                 Node("-"),                  Node("-"),                     Node("-"),      Node("-"),      Node("-")]
            ]

    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.setupLights()

        for x in range(width):
            for y in range(height):
                self.field[x][y].setCoords([x,y])

        for l in self.field:
            for n in l:
                # print("{}".format(n))
                self.addNeighbours(n)

    def addNeighbours(self, node):
        x = node.x
        y = node.y
        if node.name == '-':
            return

        for xOffset in range(-1, 2):
            for yOffset in range(-1, 2):
                try:
                    link = self.field[x + xOffset][y+yOffset]
                    if link.name != '-':
                        node.addNeighbour(link)
                except IndexError:
                    pass
                except:
                    print("Non-index error in assigning neighbours.")

    def getNode(self, name):
        for l in self.field:
            for n in l:
                if n.name == name:
                    return n


    def getValue(self, x ,y):
        return self.field[x][y].toStr()
