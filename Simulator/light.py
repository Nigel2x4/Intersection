import random
from termcolor import colored

class Light:

    def __init__(self, lid):
        self.id = lid
        self.vehicle = 0

    def getLight(self):
        raise NotImplementedError

    def setLight(self, value):
        raise NotImplementedError

    def toStr(self):
        raise NotImplementedError

    def getId(self):
        return self.id

    def getTrafficCount(self):
        if self.vehicle != 0:
            return 1
        return 0

    def getTraffic(self):
        return self.vehicle

    def setTraffic(self, vehicle):
        self.vehicle = vehicle

class CarLight(Light):
    def __init__(self, lightId):
        Light.__init__(self, lightId)
        self.__MY_VALUE = 0

    def getLight(self):
        return self.__MY_VALUE

    def setLight(self, value):
        if value == "green":
            self.__MY_VALUE = 1
        elif value == "yellow" or value == "orange":
            self.__MY_VALUE = 3
        else:
            self.__MY_VALUE = 0

    def toStr(self):
        if self.__MY_VALUE == 0:
            return colored("R",'red', attrs=['bold', 'underline'] )
        elif self.__MY_VALUE == 3:
            return colored("Y",'yellow', attrs=['underline'] )
        else:
            return colored("G",'green', attrs=['underline'])

    def getTraffic(self):
        return self.vehicle

    def setTraffic(self, param):
        self.vehicle = param

class BusLight(Light):
        def __init__(self, lightId):
            Light.__init__(self, lightId)
            self.__MY_VALUE = 0

        def getLight(self):
            return self.__MY_VALUE

        def setLight(self, value):
            self.__MY_VALUE = value

        def toStr(self):
            if self.__MY_VALUE == 0:
                return colored("R",'red',  attrs=['bold', 'underline'])
            else:
                return colored("G",'green',  attrs=['underline'])

class PedLight(Light):
        def __init__(self, lightId):
            Light.__init__(self, lightId)
            self.__MY_VALUE = 0

        def getLight(self):
            return self.__MY_VALUE

        def setLight(self, value):
            self.__MY_VALUE = value

        def toStr(self):
            if self.__MY_VALUE == 0:
                return colored("R",'red',  attrs=['bold', 'underline'])
            else:
                return colored("G",'green',  attrs=['underline'])


class BikeLight(Light):
        def __init__(self, lightId):
            Light.__init__(self, lightId)
            self.__MY_VALUE = 0


        def getLight(self):
            return self.__MY_VALUE

        def setLight(self, value):
            self.__MY_VALUE = value

        def toStr(self):
            if self.__MY_VALUE == 0:
                return colored("R",'red',  attrs=['bold', 'underline'])
            else:
                return colored("G",'green',  attrs=['underline'])

class RailLight(Light):
        def __init__(self, lightId):
            Light.__init__(self, lightId)
            self.__MY_VALUE = 0

        def getLight(self):
            return self.__MY_VALUE

        def setLight(self, value):
            self.__MY_VALUE = value

        def toStr(self):
            return (' ')
