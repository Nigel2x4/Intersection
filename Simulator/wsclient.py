# from autobahn.asyncio.websocket import WebSocketClientProtocol, \
#     WebSocketClientFactory
#
# import asyncio
# import json
# import random
# from multiprocessing import *
#
# queue = 0
#
# # JSON versturen, niet als string
#

from multiprocessing import *
import json
import websockets
import asyncio
import time

class WSocketMaster(Process):

    def __init__(self, queue):
        Process.__init__(self)
        self.queue = queue


    # arjen = ws://299ea796.ngrok.io
    # arjen2 = ws://d3bff99e.ngrok.io
    # lukas = ws://217.120.20.200:8080/ws

    def run(self):
        async def hello():
            async with websockets.connect('ws://217.120.20.200:8080/ws') as websocket:
                while True:
                    # print("A")
                    # time.sleep(0.1)
                    # await websocket.send("hot")
                    if not self.queue.empty():
                        # print("B")
                        obj = self.queue.get()
                        # print("C")
                        if obj["dest"] == "ws":
                            # print("D")
                            # print(obj["data"])
                            await websocket.send(obj["data"])
                        else:
                            self.queue.put(obj)
                    # print("E")
                    res = await websocket.recv()
                    # print(res)
                    # print("F")
                    self.queue.put({"dest":"sim","data":res})
        asyncio.get_event_loop().run_until_complete(hello())




        # factory = WebSocketClientFactory(u"ws://217.120.20.200:8080/ws")
        # factory.protocol = SlowSquareClientProtocol
        #
        # loop = asyncio.get_event_loop()
        # coro = loop.create_connection(factory, '217.120.20.200', 8080)
        # loop.run_until_complete(coro)
        # loop.run_forever()
#
#
# class SlowSquareClientProtocol(WebSocketClientProtocol, WSocketMaster):
#
#     # def onConnect(self):
#     #     WSocket.update()
#
#     def onOpen(self):
#         # Client must init conversation.
#         obj = queue.get()
#         if obj["dest"] == "ws":
#             self.sendMessage((obj["data"]).encode('utf8'))
#         else:
#             queue.put(obj)
#         # WSocketMaster.__init__(self, 0)
#         # x = '{"state" : [{"node": '++', "count":1}]}'
#         # x = json.loads(x)
#         # print(x["Message"])
#         # print("Request {} sent.".format(x))
#
#     def onMessage(self, payload, isBinary):
#         # When you get a message back, put it in the queue, and return
#         # the current status
#         if not isBinary:
#             res = json.loads(payload.decode('utf8'))
#             queue.put({"dest":"sim","data":res})
#             obj = queue.get()
#             if obj["dest"] == "ws":
#                 self.sendMessage(json.dumps(obj["data"]).encode('utf8'))
#             else:
#                 queue.put(obj)
#
#             # print("Result received: {}".format(res))
#             # print(res["state"][0]["status"])
#             #self.queue.put(res)
#
#     def onClose(self, wasClean, code, reason):
#         if reason:
#             print(reason)
#         loop.stop()
#
# if __name__ == "__main__":
#     Wsm = WSocketMaster("Succes!")
#     Wsm.start()
