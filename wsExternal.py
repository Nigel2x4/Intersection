from wsclient import *

import multiprocessing

class Stuff(Process):

    def __init__(self, q):
        Process.__init__(self)
        self.queue = q
        return

    def start(self):
        while True:
            x = input()
            self.queue.put(x)

if __name__ == "__main__":
    q = multiprocessing.JoinableQueue()

    ws = SlowSquareClientProtocol(q)
    s = Stuff(q)
    s.start()
    ws.start()



# x = json.loads(x)
# print(x["Message"])
